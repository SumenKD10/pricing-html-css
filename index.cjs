const myCheckbox = document.querySelector(".checkbox");
const priceForBasic = document.querySelector("#basicPrice");
const priceForProfessional = document.querySelector("#professionalPrice");
const priceForMaster = document.querySelector("#masterPrice");

myCheckbox.addEventListener("click", function () {
  if (myCheckbox.checked) {
    priceForBasic.innerText = "19.99";
    priceForProfessional.innerText = "24.99";
    priceForMaster.innerText = "39.99";
  } else {
    priceForBasic.innerText = "199.99";
    priceForProfessional.innerText = "249.99";
    priceForMaster.innerText = "399.99";
  }
});
